import discord
import requests
import json
import random
from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

# DISCORD BOT：https://www.youtube.com/watch?v=SPTfmiYiuok
# SQLite：https://www.runoob.com/sqlite/sqlite-python.html

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///bot.db'
db = SQLAlchemy(app)

# 資料庫模型
class response_sad_msg(db.Model):
    __tablename__ = 'response_sad_msg'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    author = db.Column(db.String(50), nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, content, author):
        self.content = content
        self.author = author

client = discord.Client()
sad_words=["失望", "難過", "不開心", "哭哭", "sad", "unhappy", "QQ"]
response_sad_words = ["怎？", "老鐵～咋滴？", "??????", "發生什麼事？"]
thank_words=["THX","thx","謝囉","謝謝","感謝","穴穴你","謝謝你","感恩","thanks"]
response_thank_words=["不會", "你蒸棒", "噁心", "多謝幾句"]


def get_quote():
    response = requests.get("https://zenquotes.io/api/random")
    json_data = json.loads(response.text)
    quote = json_data[0]['q'] + " - " + json_data[0]['a']
    return quote

def update_botmsg(msg):
    if "response_sad_words" in db.keys():
        response_sad_words = db["response_sad_words"]
        response_sad_words.append(msg)
        db["response_sad_words"] = response_sad_words
    else:
        db["response_sad_words"] = [msg]

def delete_botmsg(index):
    response_sad_words = db["response_sad_words"]
    if len(response_sad_words) > index:
        del response_sad_words[index]
        db["response_sad_words"] = response_sad_words

@client.event
async def on_ready():
    print("Logged in as {0.user}".format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    msg = message.content
    if msg.startswith('靜思語'):
        quote = get_quote()
        await message.channel.send(quote)

    elif msg.startswith('PL'):
        await message.channel.send("P你媽!") 



    options = response_sad_words
    if "response_sad_words" in db.keys():
        options = options + db["response_sad_words"]

    if any(word in msg for word in sad_words):
        await message.channel.send(random.choice(options))

    elif any(word in msg for word in thank_words):
        await message.channel.send(random.choice(response_thank_words))
    
    # 新增句子
    if msg.startswith("$new"):
        response_sad_words_msg = msg.split("$new ", 1)[1]
        update_botmsg(response_sad_words_msg)
        await message.channel.send("New msg added")
    if msg.startswith("$del"):
        response_sad_words = []
        if "response_sad_words" in db.keys():
            index = int(msg.split("$del", 1)[1])
            delete_botmsg(index)
            response_sad_words = db["response_sad_words"]
            await message.channel.send(response_sad_words)

client.run('ODc3NTg5NTE1MzAzMjU2MTA1.YR007Q.xWAosvtIfUKBinBOnE8_OdaFNWQ')